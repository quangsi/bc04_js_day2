function tinhTien() {
  var soNguoi = document.getElementById("txt-so-nguoi").value * 1;
  var phanTramTip = document.getElementById("phan-tram-tip").value * 1;
  var tongTien = document.getElementById("txt-tong-tien").value * 1;
  console.log({ soNguoi, phanTramTip, tongTien });
  var result = (tongTien * phanTramTip) / soNguoi;
  document.getElementById("result").innerHTML = `<h1>
   Số tiền phải trả là: ${result} đồng </h1>`;
  // "số tiền phải trả là: " + result + " đồng";
}

// var result = 5 + parseInt("5") * 1;
// //    chỉ có phép + sẽ tính là cộng chuỗi
// //    ép kiểu bằng cách *1
// console.log("result: ", result);
